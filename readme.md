# Simple Email Login Authentication Laravel PHP Framework

This project is another way of authenticating users without using password. The idea of this is that when you enter your email address, the system will send you an email with the link provided as a gateway to sign in.

# How to Use

Please change the env configuration. Right now I set the mail driver to Log.

# Credits
Laracast (https://laracasts.com/series/laravel-authentication-techniques/episodes/1)