<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class LoginToken extends Model
{
	protected $fillable = ['user_id','token'];

	/**
	 * Get Route Key name
	 * @return string
	 */
	public function getRouteKeyName()
	{
		return 'token';
	}

	/**
	 * Generate Token for the User
	 * @param  User   $user 
	 * @return token
	 */
	public static function generateTokenFor(User $user)
	{
		return static::create([
			'user_id' => $user->id,
			'token' => str_random(50),
			]);
	}

	/**
	 * Send Email Link to the User
	 * @return void
	 */
	public function send()
	{
		$url = url('auth/token',$this->token);

		Mail::raw("<a href='{$url}'>{$url}</a>", function ($message) {
		    $message->to($this->user->email)
		    		->subject('Your Login Link');
		});
	}

	/**
	 * Eloquent Relationship
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
