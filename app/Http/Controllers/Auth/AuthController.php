<?php

namespace App\Http\Controllers\Auth;

use Validator;
use App\Http\Controllers\Controller;
use App\AuthenticatesUser;
use App\LoginToken;

class AuthController extends Controller
{
    protected $auth;
    
    /**
     * Constructor
     * @param AuthenticatesUser $auth 
     */
    public function __construct(AuthenticatesUser $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Get Response of Login
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('login');
    }

    /**
     * Post Response of Login and sending Email link for signing in
     * @return string
     */
    public function postLogin()
    {
        $this->auth->sendEmailInvitation();
        return 'Please Check Your Email!';
    }

    /**
     * Authenticate Login Link
     * @param  LoginToken $token
     * @return string
     */
    public function authenticate(LoginToken $token)
    {
        $this->auth->login($token);
        return 'Put your Dashboard here '.\Auth::user()->name;
    }
}
