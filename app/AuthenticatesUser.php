<?php

namespace App;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;

class AuthenticatesUser
{
	use ValidatesRequests;

	protected $request;

	/**
	 * Constructor
	 * @param Request $request
	 */
	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	/**
	 * Send Email Link
	 * @return void
	 */
	public function sendEmailInvitation()
	{
		$this->validateRequest()
		->createToken()
		->send();
	}

	/**
	 * Set Login Credential and Delete old used token
	 * @param  LoginToken $token 
	 * @return void
	 */
	public function login(LoginToken $token)
	{
		\Auth::login($token->user);
		$token->delete();
	}

	/**
	 * Validate Request from the Login Form
	 * @return AuthenticatesUser
	 */
	private function validateRequest()
	{
		$this->validate($this->request,[
			'email' => 'required|email|exists:users'
			]);

		return $this;
	}

	/**
	 * Create Unique Token
	 * @return token
	 */
	private function createToken()
	{
		$user = User::byEmail($this->request->email);
		return LoginToken::generateTokenFor($user);
	}
}
